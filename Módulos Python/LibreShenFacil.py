#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  LibreShenFacil.py
#  
#  Copyright 2013 Alitux <alejandrofabrega@ingenieriapesquera.com.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#Libreshen Fácil
#El objetivo de este módulo es simplemente integrar los módulos que ya funcionan para un uso sencillo en consola
import os, re
import commands
from CargadePanos import *
from SSolida import *
from ResistenciaRed import *
from VmaxArrastre import *
Nombrearchivoelegido=""
NombredeRed=""
Cabecera=["Ninguna Red Cargada","Nadie",""]
def MenuPrincipal():
	global Nombrearchivoelegido
	global Cabecera
	global MatrizRed
	eleccion=""
	os.system("clear")
	print "---------------"
	print "GNU/Libreshen Fácil"
	print "---------------"
	print "1-Cargar Nueva Red"
	print "2-Modificar Red"
	print "3-Elegir Red ["+Cabecera[0]+" de "+Cabecera[1]+"]"
	print "4-Calcular Superficie Sólida de la Red"
	print "5-Calcular Resistencia de la Red"
	print "6-Calcular Velocidad Máxima de Arrastre"
	print "7-Salir"
	while not eleccion.isdigit() or int(eleccion)<1 or int(eleccion)>7:
		eleccion=raw_input("Elija una opción: ")
		print eleccion
	eleccion=int(eleccion)
	return eleccion
	
def BuclePrincipal():
	eleccion=MenuPrincipal()
	EleccionDeFunciones(eleccion)

def ElegirRed():
	#Elegir red con Zenity
	global NombreRed
	global Cabecera
	global MatrizRed
	Nombrearchivoelegido=commands.getoutput("zenity --file-selection --text='Elija red a abrir'")
	archivoabrir=open(Nombrearchivoelegido,"r")
	Cabecera=eval(archivoabrir.readline())
	MatrizRed=eval(archivoabrir.readline())
	archivoabrir.close()
	BuclePrincipal()


def EleccionDeFunciones(eleccion):
	os.system("clear")	
	if eleccion == 1: 
		print "----------------"
		print "CARGAR NUEVA RED"
		print "----------------"
		CargadePanos()
		BuclePrincipal()
		
	if eleccion==2:
		print "¡Jejeje! Función a Implementar"
		raw_input()
		BuclePrincipal()
		
	if eleccion==3:
		print "-----------"
		print "ELEGIR RED "
		print "-----------"
		Nombrearchivoelegido=ElegirRed()
		
		BuclePrincipal()
		
	
	if eleccion==4:
		print "-----------------------------"
		print "CALCULO DE SUPERFICIE SOLIDA "
		print "-----------------------------"
		raw_input()
		BuclePrincipal()
	if eleccion==5:
		print "---------------------------------"
		print "CALCULO DE RESISTENCIA DE LA RED "
		print "---------------------------------"
		raw_input()
		BuclePrincipal()
	if eleccion==6:
		print "----------------------------------------"
		print "CÁLCULO DE VELOCIDAD MAXIMA DE ARRASTRE "
		print "----------------------------------------"
		raw_input()
		BuclePrincipal()
	if eleccion==7:
		print "------"
		print "SALIR "
		print "------"
		return
	
	
	


BuclePrincipal()
