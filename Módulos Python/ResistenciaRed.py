#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  ResistenciaRed.py
#
#  Copyright 2013 Alitux <alejandrofabrega@ingenieriapesquera.com.ar>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
def ResistenciaRed(Ssolida,v):
	#Comprobaciones preliminares:
	if Ssolida <= 0: return "Valor inválido de Superficie Sólida"
	if v <= 0: return "Valor inválido de velocidad"
	#Cálculo final de resistencia
	ResistenciaRed=Ssolida*((0.00624+0.00475*v**2)/(1+0.0641*v))*1000  #Se aplica la Fórmula de Marine Laboratory Aberdeen
	return ResistenciaRed


