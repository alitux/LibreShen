#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  PotenciaDisponible.py
#
#  Copyright 2013 Alitux <alejandrofabrega@ingenieriapesquera.com.ar>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

def PotenciaDisponible(BHP,TipoHelice,RPM,v):
	"""Esta función calcula la Potencia de Tiro disponible en kgf.
		La función requiere 4 parámetros básicos:
		PotenciaDisponible(BHP,TipoHelice,RPM,v)
		BHP: Potencia Nominal disponible expresada en HP y con valores de trabajo entre 0 y 4000 HP inclusive
		TipoHelice: Tipo de Hélice utilizada. Puede tomar los siguientes valores:
			0 = Paso Fijo
			1 = Paso Fijo + Tobera
			2 = Paso Variable
			3 = Paso Variable + Tobera
		RPM: Debido a que no sé cual es el límite entre bajo y alto RPM, este valor puede tomar dos valores:
			0: Bajo RPM
			1: Alto RPM
		v: Velocidad expresada en nudos"""

	#Comprobaciones preliminares
	if BHP<=0 or type(BHP)!=int:
		return "Error al especificar Potencia Nominal (BHP). No puede ser nula y debe ser un valor entero positivo"
	if BHP > 4000: return "Potencia fuera de rango (valores entre 0 4000 HP)"
	if TipoHelice>3 or TipoHelice<0:
		return "Tipo de Hélice no definida"
	if RPM > 1 or RPM=="":
		return "Especificar RPM"
	if type(RPM)!=int or RPM<0:
		return "Error al especificar RPM (Sólo valores numéricos enteros entre cero y uno)"
	if v < 0:
		return "Error al especificar Velocidad (En Nudos y valores positivos)"
	
	
	# 1er Parte Elección de fórmula a utilizar para BPF
	if TipoHelice == 0: BPF=0.0099*BHP #PF (Paso Fijo)
	if TipoHelice == 1: BPF=0.0108*BHP #PFT (Paso Fijo Tobera)
	if TipoHelice == 2: BPF=0.01125*BHP #PV (Paso Variable)
	if TipoHelice == 3: BPF=0.0126*BHP #PVT (Paso Variable Tobera)
	
	# 2da parte Elección del valor de K1
	if TipoHelice == 0: #PF
		if BHP<= 500 and BHP<=2000: K1=10.5 #Valores Promedio (Se puede mejorar con proporciones)
		if BHP > 2000 and BHP <= 4000: K1=9.5  #Valores Promedio (Se puede mejorar con proporciones)
	
	if TipoHelice == 1: #PFT
		if BHP <=500 and BHP <= 2000: K1=12.25 #Valores Promedio (Se puede mejorar con proporciones)
		if BHP > 2000 and BHP <= 4000: K1=11.25 #Valores Promedio (Se puede mejorar con proporciones)
		
	if TipoHelice == 2: #PV
		if BHP <=500 and BHP <= 2000:K1=11 #Valores Promedio (Se puede mejorar con proporciones)
		if BHP > 2000 and BHP <= 4000:K1=10 #Valores Promedio (Se puede mejorar con proporciones)
	
	if TipoHelice == 3: #PVT
		if BHP <=500 and BHP <= 2000:K1=13 #Valores Promedio (Se puede mejorar con proporciones)
		if BHP > 2000 and BHP <= 4000:K1=12 #Valores Promedio (Se puede mejorar con proporciones)
			
	# 3er Parte Elección del Valor de K2 y cálculo final
	if TipoHelice==0 or TipoHelice==1:
		#Una pregunta pendiente: ¿Cuál es el criterio para que sea bajo o alto RPM?
		if RPM == 0: K2=0.265 #Para Hélice de bajo RPM (Valor Promedio)
		if RPM == 1: K2=0.20 #Para Hélice de Alto RPM
	if TipoHelice==2 or TipoHelice==3: K2=.29
	
	BPf=(10.0125*K2*K1*BHP)/v #Potencia Disponible expresada en kgf
	return BPf


