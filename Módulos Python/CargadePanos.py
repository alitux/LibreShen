#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Carga de paños.py
#
#  Copyright 2013 Akoharowen <alejandrofabrega@ingenieriapesquera.com.ar>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#
"""
¡¡¡¡MODULO PARA CARGAR POR CONSOLA!!!
MODULO de CARGA DE DATOS DE PANELES (EJEMPLO)
El objetivo de este módulo es cargar en una matriz los datos básicos de
los paneles y luego guardarlos en un archivo:
------------------------------

"""

def CargadePanos():
	#Carga de Datos de cabecera de Red
	nombre=""
	autor=""
	descred=""
	while nombre=="":
		nombre=raw_input("Nombre de la Red: ") #Alfa
	while autor=="":
		autor=raw_input("Autor: ") #Alfa
	while descred=="":
		descred=raw_input("Descripcion de la Red: ") #Alfa
	archivo=open(nombre+".red","w") 
	datosred=str([nombre,autor,descred])
	#print datosred
	archivo.write(datosred)
	archivo.write('\n')
	seguir=1
	#Carga de paños de la Red
	Red=""
	PreRed=""
	iteraciones=0
	Panel=1
	while seguir==1:
		iteraciones=iteraciones+1
		print "------------"
		print "Panel:"+str(Panel) #n+1 ó b+letra
		print "------------"
		while 1:  #Comprobacion de Enteros
			try:
				NPaneles=input("Cantidad de Paneles: ") #Entero positivo
				break
			except:
				print "Valor inesperado"
			
		Descripcion=raw_input("Descripción: ") #Alfanumérico
		Material=raw_input("Material: ") #Alfanumérico
		while 1:  #Comprobacion de Enteros
			try:
				Runnage=input("Runnage: ") #Flotante Positivo (m/kg)
				break
			except:
				print "Valor Inesperado"
		while 1:
			try:
				mL=input("mL: ") #Entero Positivo
				break
			except:
				print "Valor Inesperado"
		while 1:
			try:
				dHilo=input("dHilo: ") #mm
				break
			except:
				print "Valor Inesperado"
		while 1:
			try:
				T1=input("T1: ") #Entero Positivo
				break
			except:
				print "Valor Inesperado"
		while 1:
			try:
				T2=input("T2: ") #Entero Positivo
				break
			except:
				print "Valor Inesperado"
		while 1: 
			try:
				N2=input("N2: ") #Entero Positivo
				break
			except:
				print "Valor Inesperado"
		panel=[Panel, "Descripcion",mat,NPaneles,Runnage,mL,Dhilo,T1,T2,N2]
		seguir=raw_input("¿Cargar otro panel?")
		if seguir=="0" or seguir=="n":
			PreRed=PreRed+","+str(panel)
			Red="["+str(PreRed)+"]"	
			print datosred
			print Red
			print "-"*len("Se guardó "+ nombre+".red "+ "en forma Exitosa")
			print "Se guardó "+ nombre+".red "+ "en forma Exitosa"
			print "-"*len("Se guardó "+ nombre+".red "+ "en forma Exitosa")
			archivo.write(Red)
			archivo.close()	
		else:
			if iteraciones==1:
				PreRed=str(panel)
			else:
				PreRed=PreRed+","+str(panel)
			seguir=1
			Panel=Panel+1
			

