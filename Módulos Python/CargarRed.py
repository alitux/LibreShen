#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# LeerRed .py
#  
#  Copyright 2013 Alitux <alejandrofabrega@ingenieriapesquera.com.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
"""
Lectura de Archivos de Redes
El siguiente archivo contiene dos funciones:
NombreRed: Lee el nombre de la red
LeerRed: Utilizada para leer todo el contenido de un archivo.red y devolver el contenido en forma de matriz
Leer ParteRed: Utilizada para leer cada parte de red de un archivo.red y devolver en formato lista
"""	
def NombreRed(nombrearchivo):
#Devuelve el nombre de la red
	archivo=open(nombrearchivo)
	nombre=eval(archivo.readline())[0]
	archivo.close()	
	return nombre
def Autor(nombrearchivo):
#Devuelve el autor de la red
	archivo=open(nombrearchivo)
	autor=eval(archivo.readline())[1]
	archivo.close()	
	return autor

def Descripcion(nombrearchivo):
#Devuelve la descripción de la red
	archivo=open(nombrearchivo)
	descripcion=eval(archivo.readline())[2]
	archivo.close()	
	return descripcion



def LeerRed(nombrearchivo):
#Esta Función lee y muestra todo el archivo de la red en formato Matriz
	archivo=open(nombrearchivo)
#Pasa la primer linea de Nombre de Red
	salida=archivo.readline()
#Lee la matriz de la red
	salida=eval(archivo.readline())
	archivo.close()
	return salida

def LeerParteRed(nombrearchivo,n):
#Lee cada parte de la red. Esta función es util para cálculo de Superficie Solida y Peso.
#n=Número de Linea
	archivo=open(nombrearchivo)
#Pasa la primer linea de Nombre de Red
	total=archivo.readline()
#Lee la matriz de la red
	total=eval(archivo.readline())
#Lee la parte n de la red
	parte=total[n]
	archivo.close()		
	return parte


