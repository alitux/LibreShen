#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  SSolida.py
#  
#  Copyright 2013 Alitux <alejandrofabrega@ingenieriapesquera.com.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
def Ssolida(N2,mL,dHilo,T1,T2,NPaneles):
	
	if N2 <= 0: return "No hay valor de N2(Mallas en profundidad)"  
	if mL <= 0: return "No hay valor de mL(Mallero)"
	if dHilo <= 0 : return "No hay valor de dHilo (Diámetro del hilo)"
	if T1 <= 0 : return "No hay valor de T1(Mallas del lado mayor)"
	if T2 <= 0 : return "No hay valor de T2 (Mallas del lado menor)"
	if NPaneles <1 : return "Se debe cargar por lo menos un panel"
	
	SSolida=((N2*mL*dHilo*(T1+T2))/(1000**2))*NPaneles
	return SSolida
	

