#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  Integrador.py
#  
#  Copyright 2013 Alitux <alejandrofabrega@ingenieriapesquera.com.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import CargarRed
import PotenciaDisponible
import ResistenciaRed
import SSolida

#Calculo Superficie sólida
#Formato de Paño [Panel,NPaneles,Descripcion,Material,Runnage,mL,dHilo,T1,T2,N2]
red="Redes/FAOA1.red"
MatrizRed=CargarRed.LeerRed(red)
Ssolida=0
#Se definen las variables para Ssolida
for np in range(0,MatrizRed.__len__()):
	N2=CargarRed.LeerParteRed(red,np)[9]
	mL=CargarRed.LeerParteRed(red,np)[5]
	dHilo=CargarRed.LeerParteRed(red,np)[6]
	T1=CargarRed.LeerParteRed(red,np)[7]
	T2=CargarRed.LeerParteRed(red,np)[8]
	Npaneles=CargarRed.LeerParteRed(red,np)[1]
	#Formato Ssolida(N2,mL,dHilo,T1,T2,NPaneles)
	Ssolida=Ssolida+SSolida.Ssolida(N2,mL,dHilo,T1,T2,Npaneles)
print Ssolida
#Cálculo de Resistencia de Red
vel=0.5
#Limite de Velocidad
while vel!=5:
         vel=vel+.5
	 print str(vel) +" "+str('%.2f' %ResistenciaRed.ResistenciaRed(Ssolida,vel)) + " Kgf"

#Presentar Datos en Columna
# Recorrer las filas en la lista
num_cols = len(MatrizRed[0])
#print str(num_cols)
# Escribir las cabeceras de columna

