#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  Cortes.py
#  
#  Copyright 2013 Alejandro Fábrega <alejandrofabrega@ingenieriapesquera.com.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
def cortepano(m,M,c):
	from fractions import gcd #importamos el módulo de maximo común divisor
	y=float((M - m))/float((2*m)) #Ecuación fundamental
	print ('%.14f' %y)	#Se define si es una fracción entera o irracional
	if len(str((y)))!=14: #Condición de fracción entera
		#Se decide el tipo de corte: N o T
		mcd=gcd((M-m),(2*m)) #Se calcula el máximo común divisor
		if c == 1: #Cateto Vertical (Corte N)
			Corte=str((M-m)/mcd)+"N"+str((2*m)/mcd)+"B"
			return Corte
		if c == 2: #Cateto Horizontal (Corte T)
			Corte=str((M-m)/mcd)+"T"+str((2*m)/mcd)+"B"
			return Corte
		if c != 1 or c!=2: 
			return "ERROR: No se especifica cateto vertical u horizontal"
	else:
		#Condición irracional (dos cortes)
		Yinv = int(round((2*m)/(M-m))) #Se calcula la inversa redondeada
		CorteA="1N"+str(Yinv)+"B" #Se calcula el corte A
		VecesCorteA=(M-m)-((2*m)%(M-m)) #Cantidad de veces del corte A
		CorteB= "1N"+str(Yinv+1)+"B" #Se calcula Corte B
		VecesCorteB=((2*m)%(M-m)) #Cantidad de veces del corte B
		final=str(VecesCorteA)+" Veces  "+CorteA+"   \\\\ "+str(VecesCorteB)+" Veces  "+CorteB
		return final


print cortepano(55,90,1)


