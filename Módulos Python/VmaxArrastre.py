#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  VmaxArrastre.py
#
#  Copyright 2013 Alitux <alejandrofabrega@ingenieriapesquera.com.ar>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

def Vmax(K2,L,BHP,Ss):
	"""Teniendo en cuenta los valores de K2 (Coeficiente de RPM),L(Coeficiente del tipo de hélice),BHP y Ss, se puede determinar
	mediante iteración la velocidad máxima de arrastre.
	A su vez los valores de L pueden ser:
	PasoFijo 		==> L=1,1
	PasoFijo+Tobera 	==> L=1,2
	PasoVariable 		==> L=1,25
	PasoVariable+Tobera 	==> L=1,4
	-------------------------------------------------------------------
	SE DEBERIA PROGRAMAR UNA RUTINA QUE ELIJA AUTOMATICAMENTE EL VALOR
	-------------------------------------------------------------------
		HACER ESA TAREA ANTES DE LARGAR LA FUNCION DEFINITIVA
	-------------------------------------------------------------------
	"""
	#Se define la Cte L1
	L1=(0.091125*K2*L*BHP)/Ss
	
	#Se ponen variables a cero
	L2=0
	v=0
	while (L1-L2)>0.0005:
		L2=v*((0.00624+0.00475*v**2)/(1+0.0641*v))
		v=v+0.01 #Decimales de precisión
	return v
print Vmax(0.25,1.4,50,20)
